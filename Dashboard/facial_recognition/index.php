<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard: Reconhecimento Facial</title>
        <meta charset="utf-8" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="refresh" content="5">

        <style type="text/css">
        #cabecalho-superior{
            width: 100%;
            height: 3%;
            background-color: #005dab;
            position: absolute;
            top:0%;
            left:0%;
            display: table;
        }
        #cabecalho-inferior{
            width: 100%;
            height: 3%;
            background-color: #005dab;
            position: absolute;
            top:97%;
            left:0%;
            display: table;
        }
        #title{
            color: white;
            margin: 0;
            display: table-cell;
            position: relative;
            vertical-align: middle;
            text-align: center;
        }
        #desenvolvimento{
            color: white;
            margin: 0;
            display: table-cell;
            position: relative;
            vertical-align: middle;
            text-align: center;
        }
        #dashboard{
            color: white;
        }
        #marca-dagua{
            display: block;
            margin-left: auto;
            margin-right: auto;
            height: 450px;
            margin-top:225px;
            opacity: 0.1;
            z-index: 1;
        }
        #faces-reconhecidas{
            width: 100%;
            height: 47%;
            position: absolute;
            top:3%;
            left:0%;
        }
        #faces-nao-reconhecidas{
            width: 100%;
            height: 47%;
            position: absolute;
            top:50%;
            left:0%;
        }
        .faces{
            margin: 0px;
            position: absolute;
            z-index: 2;
        }
        .image-container{
            position: relative;
            width: 200px;
            height: 235px;
            margin: 20px 20px 5px 20px;
            float: left;
            z-index: 2;
        }
        .label{
            position: relative;
            text-align: center;
            color: black;
            font-family: Arial;
            font-size: 24px;
        }
        .rosto{
            border-radius: 20px;
        }
        .image-container-mini{
            position: relative;
            width: 100px;
            height: 117px;
            margin: 20px 20px 5px 20px;
            float: left;
            z-index: 2;
        }
        .label-mini{
            position: relative;
            text-align: center;
            color: black;
            font-family: Arial;
            font-size: 18px;
        }
        .rosto-mini{
            border-radius: 10px;
            width: 100px;
            height: 117px;
        }
        </style>
    </head>
    <body>
        <div id="cabecalho-superior">
            <p id="title">Reconhecimento facial</p>
        </div>
        <div id="dashboard">
            <img id="marca-dagua" src="QIoT.png" alt="Logo"/>
            <div id="faces-reconhecidas">
                <?php
                    $files = glob("reconhecidos/*.png");
                    if (count($files) <= 7){
                        for ($i = 0; $i < count($files); $i++):
                        
                            // Extrai o nome de acordo com o titulo do arquivo
                            $face = $files[$i];
                            $name = explode(".", explode("/", $face)[1])[0];
                            // Exibe as faces
                            $face = $files[$i];
                    ?>
                            <div class="image-container">
                                <div class = "face">
                                    <img class = "rosto" src="<?php echo $face;?>" alt="Faces reconhecidas"/>
                                </div>
                                <div class = "label">
                                    <spam><?php echo ucfirst($name); ?></spam>
                                </div>
                            </div>
                    <?php
                        endfor;
                    }
                    else {
                        for ($i = 0; $i < count($files); $i++):
                        
                            // Extrai o nome de acordo com o titulo do arquivo
                            $face = $files[$i];
                            $name = explode(".", explode("/", $face)[1])[0];
                            // Exibe as faces
                            $face = $files[$i];
                    ?>
                            <div class="image-container-mini">
                                <div class = "face">
                                    <img class = "rosto-mini" src="<?php echo $face;?>" alt="Faces reconhecidas"/>
                                </div>
                                <div class = "label-mini">
                                    <spam><?php echo ucfirst($name); ?></spam>
                                </div>
                            </div>
                    <?php
                        endfor;
                    }
                    ?>
            </div>
            <div id="faces-nao-reconhecidas">
            <?php
                    $files = glob("desconhecidos/*.png");
                    if (count($files) <= 7){
                        for ($i = 0; $i < count($files); $i++):
                        
                            // Extrai o nome de acordo com o titulo do arquivo
                            $face = $files[$i];
                            $name = explode(".", explode("/", $face)[1])[0];
                            // Exibe as faces
                            $face = $files[$i];
                    ?>
                            <div class="image-container">
                                <div class = "face">
                                    <img class = "rosto" src="<?php echo $face;?>" alt="Faces reconhecidas"/>
                                </div>
                                <div class = "label">
                                    <spam><?php echo ucfirst($name); ?></spam>
                                </div>
                            </div>
                    <?php
                        endfor;
                    }
                    else {
                        for ($i = 0; $i < count($files); $i++):
                        
                            // Extrai o nome de acordo com o titulo do arquivo
                            $face = $files[$i];
                            $name = explode(".", explode("/", $face)[1])[0];
                            // Exibe as faces
                            $face = $files[$i];
                    ?>
                            <div class="image-container-mini">
                                <div class = "face">
                                    <img class = "rosto-mini" src="<?php echo $face;?>" alt="Faces reconhecidas"/>
                                </div>
                                <div class = "label-mini">
                                    <spam><?php echo ucfirst($name); ?></spam>
                                </div>
                            </div>
                    <?php
                        endfor;
                    }
                    ?>
            </div>
        </div>
        <div id="cabecalho-inferior">
            <p id="desenvolvimento">Dashboard delevoped by Qualcomm IoT Reference Center</p>
        </div>
</html>
