# Reconhecimento facial utilizando tensorflow + pytorch + dlib


Projeto desenvolvido no IoT Reference Center, parceria entre Qualcomm e FACENS. O projeto consiste em aplicações de inteligência artificial na DragonBoard 410c para reconhecimento facial, utilizando técnicas de machine learning e deep learning.


  

# Material utilizado no projeto

- DragonBoard 410c;

- Câmera Logitech 1080p (A resolução pode afetar o desempenho durante detecção/reconhecimento).

- Monitor HDMI

- Cartão SD (mínimo 8GB, recomendado 16GB)

- PC para desenvolvimento, preferência sistema Linux

  

# Bibliotecas / Frameworks

- Python 3.5 (Dependência do TensorFlow 1.11);

- TensorFlow 1.11;

- Scipy;

- Scikit-learn;

- Dlib;

- OpenCV 3.4.3-dev + Módulos contrib.

  
# Teoria


## Etapas do Reconhecimento Facial


Procedimentos executados por uma aplicação de reconhecimento facial:

- **Detecção facial:** Processo de identificação das coordenadas de localização de uma face em uma imagem.

- **Extração de características:** Processo de definição de parâmetros que caracterizam as faces detectadas.

- **Treinamento:** Corresponde ao uso de técnicas de aprendizagem das características do **dataset de treinamento**. Nesta etapa são definidas as ferramentas utilizadas para atender os requisitos do projeto.

- **Reconhecimento facial:** Corresponde a aplicação de um modelo ou ferramenta matemática que classifica uma nova entrada com base no treinamento prévio.

  

## Detecção facial

  

Neste projeto foram realizadas duas abordagens para a detecção de faces:

1. Para uma detecção de curto alcance, a solução utiliza um modelo do framework Caffe, que consiste em uma rede neural de aprendizado profundo treinada para detecção de faces e retorno das coordenadas de posicionamento no caso positivo. O modelo de rede escolhido já é integrado com a biblioteca OpenCV (Versões > 3.3), sendo de fácil utilização e possui as seguintes características detalhadas a seguir. Reconhecimento facial realizado próximo à câmera pode ser aplicado em soluções de cadastro e controle de acesso.

- Comparado ao detector mais comum (Haar-cascade), requer mais poder de processamento, porém é muito mais preciso;

- Maior velocidade de detecção facial se comparado com o método da segunda abordagem;

- Não detecta faces em menor escala **(Distantes da câmera)**.

2. Um modelo do framework TensorFlow formado por uma rede neural convolucional (CNN, do inglês *Convolutional Neural Network*) é utilizado para a abordagem de detecção de longo alcance. Reconhecimento facial para controle de fluxo em ambientes amplos pode ser aplicado em locais abertos monitorados, como aeroportos. As características do segundo modelo:

- Necessidade de área de SWAP para a execução na DragonBoard. Veja como criar [aqui](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard(en).md).

- Maior tempo para uma detecção, aproximadamente 10 segundos por imagem.

- Detecta faces em menor escala, distantes alguns metros da câmera. No entanto, quanto maior a distância, mais distorcida será a imagem da face detectada, devido ao processo de zoom digital.

  

### Modelo Deep Neural Network construído em Caffe

Com a release do OpenCV 3.3, foi disponibilizado o módulo **dnn**. Esse módulo possibilita trabalhar com frameworks de inteligência artificial como Caffe, TensorFlow, Torch e PyTorch. Vale observar que mesmo oferecendo suporte a estes frameworks alguns modelos de redes neurais não funcionam corretamente. Para saber mais sobre o módulo dnn [acesse aqui](https://github.com/opencv/opencv/wiki/Deep-Learning-in-OpenCV).

  

Em conjunto com o módulo, também foi disponibilizado um modelo de detecção facial que pode ser encontrado no GitHub oficial do [OpenCV](https://github.com/opencv/opencv/tree/master/samples/dnn/face_detector). A rede pré-treinada através do Caffe está armazenada nos arquivos nos formatos **.prototxt** e **.caffemodel**, que devem ser utilizados como parâmtros do módulo dnn.
  

O GitHub do OpenCV disponibiliza apenas o descritor da rede, sem a inicialização dos parâmetros. Porém esses arquivos podem ser obtidos em diversos repositórios. Assim sendo, disponibilizamos esses arquivo no repositório deste projeto de reconhecimento facial. Os arquivos podem ser encontrados na pasta **modelos**, com os nomes: **faceModel.caffemodel** e **deploy.prototxt**.

  

> **ATENÇÃO:** Observe a flag swapRB, uma vez que algumas redes são treinadas com as matrizes de cores organizadas como BGR e outras como RGB.

  
Nosso projeto utiliza o modelo na bilioteca **instObjs** do diretório, criada para organizar o código. No arquivo **faceDetecting** instaciamos a classe **Face_Detector**, que utiliza o modelo para detecção. Observe nas imagens abaixo o resultado.

  

 <div align= "center">
    <figure>
        <img  src = "/imagens/fig1.png"/>
        <figcaption>Detecção facial utilizando modelo Caffe.</figcaption>
    </figure>
</div>

 <div align= "center">
    <figure>
        <img  src = "/imagens/fig2.png"/>
        <figcaption>Detecção facial utilizando modelo Caffe com faces em menor escala.</figcaption>
    </figure>
</div>

  

### Modelo Convolutional Neural Network construído em TensorFlow

Para a implementação usando TensorFlow, foi escolhido um modelo de CNN com o objetivo de detectar faces em variadas posições e escalas:

- O processo de convolução (*layers convolucionais*) é responsável por extrair características da imagem usando filtros (kernels, *feature detectors*). Como resultado, obtém-se os *features maps* que representam as posições e ocorrências das características desejadas.

- O processo de *max pooling*, aplicado nos *features maps* obtidos, diminui o tamanho dessas matrizes e torna a detecção mais precisa. As matrizes reduzidas contém as características mais importantes, tornando o modelo gerado mais robusto em relação à variação de escala das imagens. O *overfiting* é minimizado com o *max pooling* uma vez que uma parcela dos *feature maps* são descartadas, preservando apenas as características importantes.

Para visualizar melhor as etapas de uma CNN, acesse a [interface](http://scs.ryerson.ca/~aharley/vis/conv/flat.html) criada por Adam Harley.


O modelo da CNN está disponibilizado no repositório do projeto, na pasta **tensorflowObjs**, no formato **.pb**.
  

O algoritmo implementado para o projeto está localizado no diretório **tensorflowObjs**. No arquivo **mainTensor.py**, é definida a função **tf2faces** em que uma imagem é passada através rede, e o retorno é uma matriz cotendo as faces encontradas, com o grau de confiança e a posição em x e y na imagem original.

Abaixo é possível observar como a rede executa a detecção facial, com distâncias variadas em relação à câmera.

  

<div align= "center">
	<figure>
		<img  src = "/imagens/fig3.png"/>
		<figcaption>Detecção facial utilizando modelo TensorFlow.</figcaption>
	</figure>
</div>


<div align= "center">
	<figure>
		<img  src = "/imagens/fig4.png"/>
		<figcaption>Detecção facial utilizando modelo TensorFlow com faces em menor escala.</figcaption>
	</figure>
</div>

  

## Detecção dos pontos faciais e alinhamento facial (Opcional)

  

O procedimento de alinhamento permite utilizar imagens de faces que foram detectadas em posições não usuais. O processo é opicional para o projeto já que demanda certo poder de processamento, mas pode apresentar um ganho pequeno na precisão. Sua aplicação aqui é garantir que todas as imagens de faces do nosso dataset estejam alinhadas da mesma maneira.
  

O objetivo é alinhar as faces para otimizar o processo de extração das características que representam cada indivíduo. Utilizado os pontos faciais que indicam a posição da sobrancelha, dos olhos, do nariz, da boca, e do contorno do rosto em geral, detecta-se a angulação da inclinação da face, e a imagem é rotacionada para a posição padraão. Para a obtenção dos pontos citados, é utilizada a biblioteca Dlib. 


  

Para efetuar a detecção é necessário um .dat que contém o detector de prontos faciais pré-treinado (disponibilizado no repositório, na pasta **modelo**). O objeto detector deve ser inicializado através do método **shape_predictor** da biblioteca Dlib. O método recebe como parâmetro o caminho até o arquivo com extensão '.dat’. 
Após a inicialização, a imagem em escala de cinza e a coordenada da face são passadas para o objeto, através da função **dlib.rectangle**. Por fim, o retorno do modelo são 68 pontos faciais (Coordenadas x, y) que representam:

- **1  - 17:** Contorno facial.

- **18 - 22:** Sobrancelha esquerda.

- **23 - 27:** Sobrancelha direita.

- **28 - 36:** Nariz.

- **37 - 42:** Olho esquerdo.

- **43 - 48:** Olho direito.

- **49 - 68:** Boca.

  

Observe na imagem abaixo o retorno do detector de pontos faciais (Facial Landmarks detection).

  

<div align= "center">
	<figure>
		<img  src = "/imagens/fig5.png"/>
		<figcaption>Facial Landmarks detection</figcaption>
	</figure>
</div>

  

O algoritmo para este procedimento pode ser encontrado na pasta **instObjs**. O arquivo **points_embeddings.py** é responsável por carregar o detector dos pontos faciais e pela definição das funções **detectsPoints** e **drawPoints** que são responsáveis pela detecção dos pontos faciais, assim como desenha-los no frame.

Finalmente, para efetuar o alinhamento facial, utilizam-se os pontos faciais extraídos - em especial os pontos dos olhos - e ferramentas das bibliotecas Numpy e OpenCV. Além disso, as imagens das faces são redimensionadas para o processo de extração de características. O processo descrito apresenta os seguintes passos:

1. Recebe a posição dos olhos, extraindo o centróide;

2. Calcula a diferença em x e y entre os dois olhos (difX e difY);

3. Através da biblioteca numpy, calcula-se a rotação da face usando a operação arco tangente de difX por difY (CO(difY)/CA(difX));

4. Calcula o ponto central entre os olhos;

5. Cria a matriz de rotação, sendo essa dependente dos seguintes parâmetros: ponto central **(Passo 4)**, angulo **(Passo 3)** e da escala, obtida através da posição pré-definida do olho esquerdo;

6. Por último, através da função cv2.warpAffine efetua-se o alinhamento da face, sendo necessário os seguintes parâmetros: face; matriz criada no **passo 5**; Largura e altura da face (w, h); o algoritmo de interpolação utilizado para obter a imagem de saída.

  

> O algoritmo de interpolação para este procedimento pode ser encontrado no diretório **instObjs**. No arquivo **faceAlignment.py** são especificados os parâmetros que definem a configuração da imagem de saída do processo de alinhamento (função **align**). Para maior esclarecimento do processo de alinhamento, acesse o tutorial no [pyImageSearch](https://www.pyimagesearch.com/2017/05/22/face-alignment-with-opencv-and-python/).

  

<div align = "center">
	<figure>
		<img  src = "/imagens/fig6.png"/>
		<figcaption>Resultado do alinhamento (cv2.warpAffine)</figcaption>
	</figure>
</div>

  

> Os 68 pontos faciais utilizados nessa etapa são também aplicados em diversas soluções de visão computacional:

> - Detecção de emoção;

> - Detecção de embriaguez e sonolência;

> - Detecção da direção de olhar, em conjunto com a detecção da íris na imagem.

  

## Extração de features

É necessário extrair as características de cada face presente no dataset para treinar o modelo. O projeto utilizou-se da plataforma OpenFace, uma implementação de reconhecimento facial com redes de aprendizado profundo (Python e o *framework* Torch) para esta etapa. De maneira resumida, a plataforma disponibiliza modelos que possibilitam extrair da face uma representação conhecida como *embedding*, por meio de um vetor de representação hiperesférico de 128 Dimensões. Observe a imagem abaixo e para maiores informações acesse a [página da plataforma OpenFace](https://cmusatyalab.github.io/openface/).

  

<div align= "center">
	<figure>
		<img  src = "/imagens/fig7.png"/>
		<figcaption>Processo de reconhecimento facial por Dnn (Fonte: OpenFace)</figcaption>
	</figure>
</div>

  

Para nossa aplicação, foi escolhido o modelo **nn4.small2.v1**. O download pode ser realizado [aqui](https://cmusatyalab.github.io/openface/models-and-accuracies/). Para a extração dos *embeddings* é necessário primeiro carregar a rede utilizando a ferramenta **readNetFromTorch** do módulo **dnn**, do OpenCV. Uma vez carregada, para obtenção do embedding deve-se passar a face pela rede, com os canais azul e vermelho invertidos (**swapRB = True**) e a flag de crop marcada como falsa.

  

O algoritmo para este procedimento pode ser encontrado no repositório do projeto, no diretório **instObjs**, no arquivo **points_embeddings.py**. No construtor da classe é instanciada a rede para extração dos embeddings, realizada pela função detectsEmbeddings.



# Execução do projeto

# Etapa 1 - Treinamento no PC


Incialmente, instale as dependências e faça o download do repositório na sua máquina de trabalho. Para a criação do projeto utilizamos um PC com sistema Ubuntu-Linux.

Para a instalação do caffe2, siga este [tutorial](https://gitlab.com/qualcomm-iot-reference-center/mlp-for-facial-recognition-using-caffe2/blob/master/README.md).

Instale o Anaconda no seu computador. Ele vai facilitar a utilização de bibliotecas e pacotes sem afetar o ambiente da sua máquina. O download e as instruções podem ser encontrados no [site oficial](https://www.anaconda.com/).

Agora, baixe o repositório e crie um ambiente no conda para realizar os procedimentos:

```sh

git clone https://gitlab.com/qualcomm-iot-reference-center/facial-recognition-using-tensorflow-on-db410.git

cd facial-recognition-using-tensorflow-on-db410

conda create --name nome_a_sua_escolha --file spec-file.txt

conda activate nome_escolhido

```

## Construção do conjunto de treinamento

Para que o usuário que esta seguindo este guia consiga replicar os resultados, foi disponibilizado um script para a aquisição de faces para treinamento do algoritmo de reconhecimento facial. O script nomeado como **main_dataset.py** se encontra no raiz do repositório, e é responsável por iniciar um dispositivo de vídeo, detectar faces na imagem, alinhar as faces detectadas e salva-las em uma determinada pasta. 

Para a execução do script, é possível definir 3 parâmetros, sendo esses os seguintes: 

- **(-v ou --video) =>** ID da câmera a ser carregada para a aquisição dos frames;
- **(-p ou --path) =>** Diretório para salvar as faces detectadas, para posterior treinamento;
- **(-t ou --total) =>** Número de faces a salvar, antes de interromper a execução do script.

**Obs.:** Para facilitar o processo de treinamento, agrupe as imagens em um diretório para treinamento, divididas em sub-pastas para cada pessoa a ser reconhecida (**Ex.:** dataset/felipe, dataset/geovana, dataset/enzo, ...). Cada pessoa vai representar uma classe do dataset. Essa organização irá facilitar o processo de treinamento, mais especificamente a função **paths2list** que será explicada a seguir.
**Importante**: o número de imagens no dataset influencia a precisão. Armazene pelo menos 50 imagens de cada pessoa para obter um bom resultado.

Rode o script como no comando abaixo, com os parâmetros desejados. Lembre-se que o comando deve ser rodado para cada pessoa que será cadastrada:

```sh

python3 main_dataset.py -v 0 -p "./dataset/joão" -t 50
```

## Treinamento da rede

> No repositório do projeto, o arquivo python **paths2list** localizado na pasta **instObjs**, define a função **pathsIMG**, que possui as seguintes funcionalidades:

> - Recebe como parâmetro o diretório do dataset. O esperado é que ele esteja dividido em classes, como explicado anteriormente;

> - Retorna duas listas: uma contendo o caminho para cada imagem do dataset e outra contendo a label de classe de cada imagem (Nomes das pessoas).

Utilizamos uma aplicação do Jupyter Notebook para facilitar a reprodução do projeto. O Jupyter é uma biblioteca do Python que permite rodar código em uma interface web dividido em células, facilitando o compartilhamento e prototipagem de programas. Caso precise de instruções de como usar, este [link](https://www.youtube.com/watch?v=HW29067qVWk) é um bom começo.

Quando estiver familiarizado com a ferramenta e com o ambiente do anaconda ainda ativo, abra o arquivo **mainTR.ipynb**, disponibilizado na raiz do repositório do projeto. Execute todas as células, fique atento para o caminho do dataset que deve ser substituído pelo seu. No final da execução, o código irá gerar dois arquivos na pasta local: **faces128.pickle** e **labels128.pickle**, que contém o modelo de reconhecimento e as labels das classes para o seu dataset.

O modelo escolhido para o reconhecimento foi um algoritmo de SVM. Foram realizados testes com algoritmos presentes na biblioteca sklearn, e dentre os testados (decision trees, SVM, KNN), o SVM apresentou uma taxa de acerto satisfatória para o nosso dataset.


# Etapa 2 - Instalação na Dragonboard

O primeiro passo para a instalação do projeto é a instalação das bibliotecas na placa. Siga o tutorial [aqui](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/opencv_installation(en).md) para instalar o OpenCV e o Python 3.5, e [aqui](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/aiFrameworks(en).md) com todo o passo a passo necessário para a instalação das bibliotecas.

## Webpage para exibição do Dashboard
 
Para o nosso projeto, utilizamos uma interface web para exibir faces detectadas e reconhecidas, para fins de demonstração na demoroom. O dashboard foi desenvolvido em php e funciona efetuando a leitura de duas pastas no sistema da dragonboard, que armazenam as faces reconhecidas e desconhecidas. As imagens são salvas nas pastas durante a execução do programa principal.

O código da webpage foi disponibilizado no repositório do projeto, no diretório **/Dashboard/facial_recognition**. Para ter sua aplicação rodando na DragonBoard como web server, siga os seguintes passos de instalação, executados no terminal da Dragonboard:

1. Instale as dependências, php e apache2:
	```

	sudo apt-get install php7.3 php-pear

	sudo apt-get install apache2

	```

2.  Clone o repositório do projeto na placa. Copie a pasta **facial_recognition** e todo seu conteúdo para o diretório **/var/www/html/**, com o comando abaixo:
	```sh

	git clone https://gitlab.com/qualcomm-iot-reference-center/facial-recognition-using-tensorflow-on-db410.git
	cd facial-recognition-using-tensorflow-on-db410

	sudo cp -r ./Dashboard/facial_recognition /var/www/html/

	```

3. Para que a Dragonboard atue como webserver, basta mantê-la conectada à internet. Para acessar a placa e visualizar o conteúdo, é preciso conhecer o endereço IP. Execute o comando **ip addr** no terminal da Dragonboad para descobrir o endereço na rede, que é indicado no campo **inet** apresentado na tela após a execução do comando. (**Ex.:** inet 192.168.1.101) 
4. Com a placa conectada e o endereço IP em mãos, para acessar a web page utilize um navegador de qualquer dispositivo, que deve estar conectado **na mesma rede que a placa**, digite o endereço na barra de navegação como indicado: **ip/facial_recognition** (**Ex.:** 192.168.1.101/facial_recognition)

## Executando o reconhecimento facial

Antes de executar o modelo, é necessário copiar os arquivos gerados durante o treinamento para dentro da Dragonboard. A maneira mais simples é utilizar um pendrive, copie os arquivos do PC (**faces128.pickle** e **labels128.pickle**) e coloque-os no repositório do projeto, dentro da placa.

O script principal do projeto, presente na raiz do diretório com o nome de **main.py**, é responsável por executar a detecção e reconhecimento facial. 

O fluxo de funcionamento do algoritmo desenvolvido:

1. Divisão das tarefas em dois processos a serem executados pela CPU. Um processo realiza a leitura dos frames da câmera, e outro roda o algoritmo para uma imagem capturada pelo primeiro, atualizada a cada 30 segundos;
2. Detecção de faces na imagem processada pelo algoritmo, através da função **tf2facens**;
3. Extração dos pontos faciais de cada face detectada através do método **detectsPoints** da classe **signatureDetector**;
4. Alinhamento da imagem de cada face para padronização, através do método **align** da classe **fAl**;
5. Classificação do alinhamento, para detectar faces mal posicionadas que seriam reconhecidas de forma errada (Através da função **check**);
6. Faces com alinhamento ruim não passam pelo processo de reconhecimento, e são salvas diretamento no diretório **/var/www/html/facial_recognition/desconhecidos**;
7. Faces com um alinhamento válido passam pelo processo de reconhecimento: são extraídos os embeddings faciais, através do método **detectsEmbeddings** da classe **signatureDetector**;
8. Através do método **recognizes**, da classe **fRecognizer** os embeddings são classificados, através do modelo SVM, retornando a label com o nome da pessoa e a confiança (%) da classificação;
9. A imagem e o nome são salvos no diretório  **/var/www/html/facial_recognition/reconhecidos**;
10. Para exibição no dashboard o código verifica o conteúdo das pastas e atualiza os dados na interface WEB.


Para executar a aplicação, execute o script como indicado, indicando os arquivos de treinamento que você copiou para a placa:

```sh

	python main.py -c <câmera> -m <caminho_do_modelo> -l <caminho_das_labels>

```
Na imagem abaixo exibimos a tela da interface web:


<div align= "center">
	<figure>
		<img  src = "/imagens/fig8.png"/>
		<figcaption>Dashboard para exibição das faces reconhecidas</figcaption>
	</figure>
</div>

## Resumo: Passo a passo para execução
 

Para facilitar a replicação do tutorial aqui apresentado, é indicado que os seguintes passos sejam executados:

1. Construa um dataset de treinamento utilizando o script **main_dataset.py**. O script aceita os seguintes argumentos, e deve ser executado no PC:
	
	- **-v =>** Id da câmera a ser utilizada (normalmente 0);
	- **-p =>** Diretório para salvar as fotos;
	- **-t =>** Quantidade total de fotos salvas.

2. Organize seu dataset conforme indicado na seção **Construção do conjunto de treinamento** deste tutorial;
3. Utilizando o Jupyter, acesse o notebook **mainTR.ipynb** para treinar um modelo para as fotos aquisitadas;
4. Conforme indicado na seção  **Webpage para exibição do Dashboard**, habilite a DragonBoard como um WebServer;
5. Execute a aplicação principal, através do script **main.py**. O script aceita os seguintes argumentos:
	
	- **-c =>** Id da câmera;
	- **-m =>** Diretório para carregar o modelo SVM responsável pela classificação facial (Reconhecimento);
	- **-l =>** Diretório para carregar as labels do modelo SVM treinado.

