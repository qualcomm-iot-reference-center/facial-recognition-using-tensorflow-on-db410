# Facial Recognition using tensorflow + pytorch + dlib


Project developed in the IoT Reference Center, a partnership between Qualcomm and FACENS. The project consists in a application of artificial intelligence on the DragonBoard 410c for facial recognition, using techniques of machine learning and deep learning.


  

# Material used in the project

- DragonBoard 410c;

- Webcam Logitech 1080p (Resolution may affect performance during detection/recognition).

- HDMI monitor

- SD card (8GB at least, 16GB recommended)

- PC for development, Linux system preference

  

# Libraries/Frameworks

- Python 3.5 (TensorFlow 1.11 dependency);

- TensorFlow 1.11;

- Scipy;

- Scikit-learn;

- Dlib;

- OpenCV 3.4.3-dev + contrib modules.

  
# Theory


## Facial Recognition steps


Procedures performed by a facial recognition application:

- **Face Detection:** Identification process of the location coordinates of a face in an image.

- **Extraction of characteristics:** Process of defining parameters that characterize the detected faces.

- **Training:** Corresponds to the use of learning techniques of the characteristics of the **training dataset**. In this step the tools used to meet the project requirements are defined.

- **Facial Recognition:** Corresponds to the application of a mathematical model or tool that classifies a new entry based on previous training.

  

## Facial detection

In this project two approaches were realized for the detection of faces:

1. For short-range detection, the solution uses a model of the Caffe framework, which consists of a deep learning neural network trained for face detection and return the positioning coordinates in the positive case. The chosen network model is already integrated with the OpenCV library (Versions> 3.3), it's easy to use and has the following detailed characteristics. Facial recognition performed near the camera can be applied in registration and access control solutions.

- Compared to the most common detector (Haar-cascade), it requires more processing power, but is much more precise;

- Higher speed of facial detection compared to the second approach method;

- Does not detect faces on a smaller scale **(Far from the camera)**.

2. A convolutional neural network (CNN) built with TensorFlow framework is used for the long-range detection approach. Facial recognition for flow control in wide environments can be applied in monitored open locations such as airports. The characteristics of the second model:

- Need a SWAP memory area for running on DragonBoard. [Here's](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard(en).md) how to create it.

- Longer detection time, approximately 10 seconds per image.

- Detects faces on a smaller scale, some fts away from the camera. However, the greater the distance, the more distorted the image of the detected face, due to the digital zoom process.

  

### Caffe Deep Neural Network model

With the release of OpenCV 3.3, the **dnn** module was made available. This module makes it possible to work with artificial intelligence frameworks such as Caffe, TensorFlow, Torch and PyTorch. It is worth noting that even while supporting these frameworks, some neural network models do not work correctly. To learn more about the dnn module [access here](https://github.com/opencv/opencv/wiki/Deep-Learning-in-OpenCV).

  

In conjunction with the module, a facial detection model was also available, and can be found on the official [GitHub](https://github.com/opencv/opencv/tree/master/samples/dnn/face_detector) of OpenCV. The network pre-trained through Caffe is stored in the files in the formats **.prototxt** and **.caffemodel**, which should be used as parameters of the dnn module.
  

OpenCV's GitHub only provides the network descriptor without initializing the parameters. However, these files can be obtained from several repositories. Therefore, we make these files available in the repository of this facial recognition project. The files can be found in the **templates** folder, with the names: **faceModel.caffemodel** and **deploy.prototxt**.

  

> **ATTENTION:** Note the swapRB flag, since some networks are trained with color matrices organized as BGR and others as RGB.

  
Our project uses the template in the directory **instObjs**, created to organize the code. In the file **faceDetecting** we created the class **Face_Detector**, which uses the model for detection. Note in the pictures below the result.

  

 <div align= "center">
    <figure>
        <img  src = "/imagens/fig1.png"/>
        <figcaption>Facial detection using Caffe model.</figcaption>
    </figure>
</div>

 <div align= "center">
    <figure>
        <img  src = "/imagens/fig2.png"/>
        <figcaption>Caffe facial detection with smaller faces.</figcaption>
    </figure>
</div>

  

### Convolutional Neural Network TensorFlow model

For the implementation using TensorFlow, a CNN model was chosen in order to detect faces at various positions and scales:

- The convolutional process is responsible for extracting features of the image using filters (kernels, *feature detectors*). As a result, we obtain the *features maps* that represent the positions and occurrences of the desired characteristics.

- The max pooling process, applied to the obtained map features, decreases the size of these matrices and makes the detection more accurate. Reduced matrices contain the most important characteristics, making the generated model more robust in relation to the scale variation of the images. *Overfiting* is minimized with *max pooling* once a portion of *feature maps* is discarded, preserving just the most important features.

To better visualize the steps of a CNN, go to [interface](http://scs.ryerson.ca/~aharley/vis/conv/flat.html)created by Adam Harley.


The CNN model is available in the project repository, in the **tensorflowObjs** folder, in the format **.pb**.
  

The algorithm implemented for the project is located in the **tensorflowObjs** directory. In the file **mainTensor.py**, the function **tf2Faces** in which an image is passed through the network is defined, and the return is an array by matching the faces found, with the degree of confidence and the position in x and y in original image.

Below you can see how the network performs facial detection, with varying distances from the camera.

  

<div align= "center">
	<figure>
		<img  src = "/imagens/fig3.png"/>
		<figcaption>Facial detection, TensorFlow model.</figcaption>
	</figure>
</div>


<div align= "center">
	<figure>
		<img  src = "/imagens/fig4.png"/>
		<figcaption>Facial detection with TensorFlow model, greater distance detection.</figcaption>
	</figure>
</div>

  

## Face Points Detection and Face Alignment (Optional)

  

The alignment procedure allows you to use images of faces that have been detected in unusual positions. The process is optional for the project since it demands certain processing power, but may present a small gain in accuracy. Your application here is to ensure that all the faces of our dataset are aligned in the same way.
  

The objective is to align the faces to optimize the extraction process of the characteristics that represent each individual. Using the facial points that indicate the position of the eyebrow, eyes, nose, mouth, and face contour in general, the angle of the face tilt is detected, and the image is rotated to the default position. To obtain the above points, the Dlib library is used. 

To perform the detection you need a .dat file containing the pre-trained prefetch detector (available in the repository, in the **model** folder). The detector object must be initialized through the **shape_predictor** method of the Dlib library. The method takes as a parameter the path to the file with extension '.dat'. 
After initialization, the grayscale image and face coordinate are passed to the object via the function **dlib.rectangle**. Finally, the return of the model is 68 facial points (Coordinates x, y) that represent:

- **1  - 17:** Facial contour.

- **18 - 22:** Left eyebrow.

- **23 - 27:** Right eyebrow.

- **28 - 36:** Nose.

- **37 - 42:** Left eye.

- **43 - 48:** Right eye.

- **49 - 68:** Mouth.

  

Look in the image below for the Facial Landmarks detection.

  

<div align= "center">
	<figure>
		<img  src = "/imagens/fig5.png"/>
		<figcaption>Facial Landmarks detection</figcaption>
	</figure>
</div>

  

The algorithm for this procedure can be found in the **instObjs** folder. The **points_embeddings.py** file is responsible for loading the facial points detector and defining the **detectsPoints** and **drawPoints** functions that are responsible for detecting facial points, as well as drawing them in the frame.

Finally, to perform the facial alignment, the extracted facial points - in particular the eye points - and tools of the Numpy and OpenCV libraries are used. In addition, the face images are resized for the feature extraction process. The described process presents the following steps:

1. Receives the position of the eyes, extracting the centroid;

2. Calculate the difference in x and y between the two eyes (difX and difY);

3. Through the numpy library, face rotation is calculated using the arc tangent difX by difY (CO(difY)/CA(difX));

4. Calculates the center point between the eyes;

5. It creates the rotation matrix, which depends on the following parameters: center point **(Step 4)**, angle **(Step 3)** and the scale, obtained through the predefined position of the left eye;

6. Finally, through the function cv2.warpAffine the face alignment is performed, the following parameters being necessary: ​​face; matrix created in **step 5**; Face width and height (w, h); the interpolation algorithm used to obtain the output image.

  

> The interpolation algorithm for this procedure can be found in the **instObjs** directory. In the file **faceAlignment.py** the parameters that define the output image configuration of the alignment process (function **align**) are specified. For further clarification of the alignment process, access the tutorial on [pyImageSearch](https://www.pyimagesearch.com/2017/05/22/face-alignment-with-opencv-and-python/).

  

<div align = "center">
	<figure>
		<img  src = "/imagens/fig6.png"/>
		<figcaption>Results (cv2.warpAffine)</figcaption>
	</figure>
</div>

  

> The 68 facial points used in this step are also applied in several computer vision solutions:

> - Detection of emotion;

> - Detection of drunkenness and drowsiness;

> - Detection of the look direction, together with the detection of the iris in the image.

  

## Feature extraction

It is necessary to extract the characteristics of each face present in the dataset to train the model. The project utilized the OpenFace platform, an implementation of facial recognition with deep learning networks (Python and the *framework* Torch) for this stage. In summary, the platform offers models that make it possible to extract from the face a representation known as *embedding*, by means of a hyperspherical representation vector of 128 Dimensions. Look at the image below and for more information access the [OpenFace page](https://cmusatyalab.github.io/openface/).

  

<div align= "center">
	<figure>
		<img  src = "/imagens/fig7.png"/>
		<figcaption>Dnn facial recognition example (Source: OpenFace)</figcaption>
	</figure>
</div>

  

For our application, the model **nn4.small2.v1** was chosen. The download can be done [here](https://cmusatyalab.github.io/openface/models-and-accuracies/). To extract the *embeddings* it is necessary to load the network first using the **readNetFromTorch** tool of the **dnn** module of the OpenCV. Once loaded, to obtain the embedding, the face must be passed through the network, with the blue and red channels inverted  (**swapRB = True**) and the crop flag marked false.

  

The algorithm for this procedure can be found in the project repository, in the **instObjs** directory, in the **points_embeddings.py** file. In the constructor of the class is instantiated the network for extraction of the embeddings, realized by the function detectsEmbeddings.



# Project execution

# Step 1 - Training the model on PC


Initially, install the dependencies and download the repository to your working machine. For the creation of the project we use a PC with Ubuntu-Linux system.

For the installation of caffe2, follow this [tutorial](https://gitlab.com/qualcomm-iot-reference-center/mlp-for-facial-recognition-using-caffe2/blob/master/README.md).

Install Anaconda on your computer. It will make it easier to use libraries and packages without affecting the environment of your machine. The download and instructions can be found on the [official website](https://www.anaconda.com/).

Now, download the repository and create a conda environment to perform the procedures:

```sh

git clone https://gitlab.com/qualcomm-iot-reference-center/facial-recognition-using-tensorflow-on-db410.git

cd facial-recognition-using-tensorflow-on-db410

conda create --name chose_a_name --file spec-file.txt

conda activate chosen_name

```

## Construction of the training set

In order for the user who is following this guide to replicate the results, a face acquisition script for the facial recognition algorithm has been made available. The script named **main_dataset.py** is in the root of the repository, and is responsible for starting a video device, detecting faces in the image, aligning detected faces, and saving them in a given folder. 

For the execution of the script, it is possible to define 3 parameters, these being the following: 

- **(-v ou --video) =>** Camera ID for capture the images;
- **(-p ou --path) =>** path to a folder to store your photos;
- **(-t ou --total) =>** number of photos taken.

**Note:** To facilitate the training process, group images into a training directory, divided into sub-folders for each person to be recognized (**Ex .:** dataset/Jon, dataset/Julia, dataset/Mark, ...). Each person will represent a class of the dataset. This organization will facilitate the training process, more specifically the ** paths2list ** function that will be explained next.
**Important**: The number of images in the dataset influences accuracy. Store at least 50 pictures of each person to get a good result.

Run the script as in the command below, with the desired parameters. Remember that the command must be run for each person who will be registered:

```sh

python3 main_dataset.py -v 0 -p "./dataset/Jon" -t 50
```

## Net model training

> In the project repository, the python file **paths2list** located in the folder **instObjs**, defines the function **pathsIMG**, which has the following functionalities:

> - It receives as parameter the directory of the dataset. What is expected is that it is divided into classes, as explained previously;

> - Returns two lists: one containing the path to each image in the dataset and another containing the class label of each image (Names of people).

We use a Jupyter Notebook application for easy project replication. Jupyter is a Python library that allows you to run code in a web interface divided into cells, making it easy to share and prototype programs. If you need instructions on how to use it, this [link](https://www.youtube.com/watch?v=HW29067qVWk) is a good start.

Once you are familiar with the tool and with the anaconda environment still set active, open the **mainTR.ipynb** file, which is available in the root of the project repository. Run all cells, stay tuned for the path of the dataset that should be replaced by yours. At the end of the run, the code will generate two files in the local folder: **faces128.pickle** and **labels128.pickle**, which contains the recognition template and class labels for your dataset.

The model chosen for recognition was an SVM algorithm. We performed tests with algorithms present in the sklearn library, and among the tested (decision trees, SVM, KNN), the SVM presented a satisfactory hit rate for our dataset.


# Step 2 - Installing on Dragonboard

The first step to run the project is install the libraries on the board. Follow the tutorial [here](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/opencv_instalation(en).md) to install OpenCV and Python 3.5, and [here](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/aiFrameworks(en).md) for the step-by-step installation of the libraries.

## Webpage for Dashboard View
 
For our project, we use a web interface to display detected and recognized faces for demonstration purposes in the demoroom. The dashboard was developed in php and works by reading two folders in the dragonboard system, which store the recognized and unknown faces. Images are saved to folders during the execution of the main program.

The webpage code has been made available in the project repository in the **/Dashboard/facial_recognition** directory. To have your application running on DragonBoard as a web server, follow the following installation steps, on the Dragonboard terminal:

1. Install the dependencies, php e apache2:
	```

	sudo apt-get install php7.3 php-pear

	sudo apt-get install apache2

	```

2.  Clone the project repository on the board. Copy the folder **facial_recognition** and all its contents to the **/var/www/html/** directory with the command below:

	```sh

	git clone https://gitlab.com/qualcomm-iot-reference-center/facial-recognition-using-tensorflow-on-db410.git
	cd facial-recognition-using-tensorflow-on-db410

	sudo cp -r ./Dashboard/facial_recognition /var/www/html/

	```

3. For Dragonboard act as webserver, just keep it connected to the internet. To access the board and view the content, you must know it's IP address. Run the **ip addr** command on the Dragonboad terminal to find the address on the network, which is indicated in the **inet** field displayed on the screen after command execution. (**Ex.:** inet 192.168.1.101) 
4. With the board connected and the IP address in hand, to access the web page use a browser of any device, which must be connected **on the same internet network as the card**, enter the address in the navigation bar as indicated: **IP/facial_recognition** (**Ex.:** 192.168.1.101/facial_recognition)

## Performing Face Recognition

Before running the model, it is necessary to copy the files generated during the training process into the Dragonboard. The simplest way is to use a pendrive, copy the PC files (**faces128.pickle** and **labels128.pickle**) and place them in the project repository, inside the board.

The main script of the project, present in the root directory named **main.py**, is responsible for performing face detection and recognition. 

The operating flow of the developed algorithm:

1. Division of tasks into two processes to be executed by the CPU. One process reads the frames of the camera, and another runs the algorithm for an image captured by the first, refreshed every 30 seconds;
2. Detection of faces in the image processed by the algorithm, through the function **tf2facens**;
3. Extraction of the facial points of each face detected through the **detectsPoints** method of the class **signatureDetector**;
4. Alignment of each of the face images for standardization, using the **align** method of the class **fAl**;
5. Alignment classification to detect poorly positioned faces that would be wrongly recognized (Using **check** function);
6. Faces with poor alignment do not go through the recognition process, and are saved directing in the directory **/var/www/html/facial_recognition/desconhecidos**;
7. Faces with a valid alignment go through the recognition process: the facial embeddings are extracted through the **detectsEmbeddings** method of the class **signatureDetector**;
8. Through the **recognizes** method of the class **fRecognizer** the embeddings are sorted through the SVM model, returning the label with the name of the person and the confidence (%) of the classification;
9. The image and the name are stored in the  **/var/www/html/facial_recognition/reconhecidos**;
10. For display in the dashboard, the code checks the contents in the folders and updates the data in the WEB interface.


To run the application, run the script as shown, indicating the training files you copied to the board:

```sh

	python main.py -c <camera> -m <model_path> -l <labels_path>

```
In the image below we display the web interface screen:


<div align= "center">
	<figure>
		<img  src = "/imagens/fig8.png"/>
		<figcaption>Dashboard</figcaption>
	</figure>
</div>

## Summary: Step by Step for Execution
 

To facilitate the replication of the tutorial presented here, it is recommended that the following steps be performed:

1. Construct a training dataset using the **main_dataset.py** script. The script accepts the following arguments, and should run on the PC:
	
	- **-v =>** Camera ID (default = 0);
	- **-p =>** Folder to store the photos;
	- **-t =>** How many photos will be taken.

2. Organize your dataset as indicated in the **Construction of the training set** section of this tutorial;
3. Using Jupyter, access the **mainTR.ipynb** notebook to train a model for the purchased photos;
4. As indicated in the **Webpage for Dashboard View** section, enable DragonBoard as a WebServer;
5. Run the main application through the **main.py** script. The script accepts the following arguments:
	
	- **-c =>** Camera ID;
	- **-m =>** Path to the SVM model;
	- **-l =>** Path to the labels file.

