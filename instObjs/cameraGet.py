import cv2
from datetime import datetime

class CameraCapture():
    def __init__(self, cam_id):
        self.vobj = cv2.VideoCapture(cam_id)
    def run(self, q):
        start = datetime.now()
        while True:
            _, frame = self.vobj.read()
            cv2.imshow("Camera capture", frame)
            key = cv2.waitKey(1) & 0xFF
            if key == ord("q"):
                break
            end = datetime.now()
            if (end - start).total_seconds() > 30:
                q.put(frame)
                start = datetime.now()
        self.vobj.release()
        q.put(None)



