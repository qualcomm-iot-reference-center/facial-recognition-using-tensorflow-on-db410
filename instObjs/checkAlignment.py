import pickle
import cv2
import numpy as np


def check(check_model_Path, images_to_check):
    model = pickle.loads(open(check_model_Path, "rb").read())
    hog = cv2.HOGDescriptor()
    valid_images = []
    invalid_images = []

    for i in range(len(images_to_check)):
        
        h = hog.compute(images_to_check[i])
        h = np.array(h).reshape(1, 680400)
        print(h.shape)
        pred = model.predict(h)

        if pred == "valid":
                valid_images.append(images_to_check[i])
        else:
                invalid_images.append(images_to_check[i])
    
    return valid_images, invalid_images
