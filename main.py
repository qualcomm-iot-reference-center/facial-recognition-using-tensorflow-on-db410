import cv2
import pickle
import numpy as np
import os
import argparse

from instObjs.faceAlignment import Face_Aligner
from instObjs.cameraGet import CameraCapture
from instObjs.points_embeddings import FacialSignature_Detector
from instObjs.faceRecognition import Face_Recognition
from instObjs import checkAlignment
from tensorflowObjs.mainTensor import tf2faces

import multiprocessing
from multiprocessing import Queue

arg = argparse.ArgumentParser()
arg.add_argument("-c", "--cam", type = int, help = "ID of camera device")
arg.add_argument("-m", "--model", default = 'modelRecog/modelo.pickle', help = "Path to the SVM model as a pickle file")
arg.add_argument("-l", "--label", default = 'modelRecog/labels.pickle', help = "Path to the labels of the model as a pickle file")
args = vars(arg.parse_args())

ptModelPath = "modelo/shape_predictor_68_face_landmarks.dat"
embeddingPath = "modelo/nn4.small2.v1.t7"
checkmodelPath = "modelo/alignment_check.pickle"
fAl = Face_Aligner()
fRecognizer = Face_Recognition(args["model"], args["label"])
signatureDetector = FacialSignature_Detector(ptModelPath, embeddingPath)

frames_queue = Queue(maxsize = 0)
p = multiprocessing.Process(target = CameraCapture(args["cam"]).run, args = (frames_queue, ))
p.start()

def erase():
    root = "/var/www/html/facial_recognition"
    recog = "/reconhecidos"
    unrecog = "/desconhecidos"
    for file_recog in os.listdir(root + recog):
        os.remove(root + recog + "/" + file_recog)
    for file_unrecog in os.listdir(root + unrecog):
        os.remove(root + unrecog + "/" + file_unrecog)

def update_folder(names, confidences, invalidFaces):
    i = 0
    for n, c in zip(names, confidences):
        cv2.imwrite("/var/www/html/facial_recognition/reconhecidos/{}-{}.png".format(n.capitalize(), round(c * 100, 0)), facesAligned[i])
        i += 1
    for j in range(len(invalidFaces)):
        cv2.imwrite("/var/www/html/facial_recognition/desconhecidos/Desconhecido-{}.png".format(j + 1), invalidFaces[j])

def draw_faces(frame, pos):
    for p in pos:
        cv2.rectangle(frame, (p[0], p[1]), (p[2], p[3]), (0, 0, 255), 2)
        cv2.imshow("TensorFlow Output", frame)
    cv2.waitKey(5000)

erase()

while True:
    frame = frames_queue.get()
    if frame is None:
        break
    retFaces, _, posFaces = tf2faces(frame)
    fPoints = signatureDetector.detectsPoints(frame, posFaces)
    facesAligned = fAl.align(frame, fPoints)
    validFaces, invalidFaces = checkAlignment.check(checkmodelPath, facesAligned)
    fEmbeddings = signatureDetector.detectsEmbeddings(facesAligned, False)

    names, confidences = fRecognizer.recognizes(fEmbeddings)
    draw_faces(frame, posFaces)

    erase()
    update_folder(names, confidences, invalidFaces)