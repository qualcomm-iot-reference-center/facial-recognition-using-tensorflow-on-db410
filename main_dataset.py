import cv2
from tensorflowObjs.mainTensor import tf2faces
from instObjs.faceAlignment import Face_Aligner
from instObjs.points_embeddings import FacialSignature_Detector
import argparse

ptModelPath = "modelo/shape_predictor_68_face_landmarks.dat"
embeddingPath = "modelo/nn4.small2.v1.t7"
signatureDetector = FacialSignature_Detector(ptModelPath, embeddingPath)
fAl = Face_Aligner()

arg = argparse.ArgumentParser()
arg.add_argument("-v", "--video", required = False, help = "Câmera ID to the application", type = int, default = 0)
arg.add_argument("-p", "--path", required = False, help = "Path to output the aligned and scaled faces", default = ".")
arg.add_argument("-t", "--total", required = False, help = "Number of faces captured",  type = int, default = 5)
args = vars(arg.parse_args())
print(args["video"])

vobj = cv2.VideoCapture(args["video"])
n_img = 0

while True:
    _, img = vobj.read()

    retFaces, conf, posFaces = tf2faces(img)
    fPoint = signatureDetector.detectsPoints(img, posFaces)
    faces_aligned = fAl.align(img, fPoint)

    for i, p in enumerate(posFaces):
        n_img += 1
        cv2.imwrite("{}/img{}.png".format(args["path"], n_img), faces_aligned[i])
        text = "{}%".format(float(conf[i])*100)
        yTxt = p[1] - 10 if p[1] - 10 > 10 else p[1] + 10
        cv2.putText(img, text, (p[0], yTxt), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1)
        cv2.rectangle(img, (p[0], p[1]), (p[2], p[3]), (255, 0, 0), 1)
    
    cv2.imshow("Face points output", img)
    key = cv2.waitKey(1) & 0xFF

    if key == 27:
        break

    if n_img >= (args["total"]):
        break

vobj.release()
cv2.destroyAllWindows()