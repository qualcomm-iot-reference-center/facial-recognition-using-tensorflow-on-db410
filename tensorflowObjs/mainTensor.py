import numpy as np
import os
import sys
import tarfile
import tensorflow as tf
import cv2
#from matplotlib import pyplot as plt
from PIL import Image
from tensorflowObjs.utils import label_map_util

def run_inference_for_single_image(image, graph):
  with graph.as_default():
    with tf.Session() as sess:
      # Get handles to input and output tensors
      ops = tf.get_default_graph().get_operations()
      all_tensor_names = {output.name for op in ops for output in op.outputs}
      tensor_dict = {}
      for key in [
          'num_detections', 'detection_boxes', 'detection_scores',
          'detection_classes', 'detection_masks'
      ]:
        tensor_name = key + ':0'
        if tensor_name in all_tensor_names:
          tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
              tensor_name)
      if 'detection_masks' in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[0], image.shape[1])
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        # Follow the convention by adding back the batch dimension
        tensor_dict['detection_masks'] = tf.expand_dims(
            detection_masks_reframed, 0)
      image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

      # Run inference
      output_dict = sess.run(tensor_dict,
                             feed_dict={image_tensor: np.expand_dims(image, 0)})

      # all outputs are float32 numpy arrays, so convert types as appropriate
      output_dict['num_detections'] = int(output_dict['num_detections'][0])
      output_dict['detection_classes'] = output_dict[
          'detection_classes'][0].astype(np.uint8)
      output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
      output_dict['detection_scores'] = output_dict['detection_scores'][0]
      if 'detection_masks' in output_dict:
        output_dict['detection_masks'] = output_dict['detection_masks'][0]
  return output_dict

detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile("tensorflowObjs/frozen_inference_graph_face.pb", 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

category_index = label_map_util.create_category_index_from_labelmap("tensorflowObjs/face_label_map.pbtxt", use_display_name=True)

def tf2faces(img):
    indexes = []
    faces = []
    conFaces = []
    posFaces = []
    newImg = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    (h, w) = newImg.shape[0:2]
    
    output_dict = run_inference_for_single_image(newImg, detection_graph)

    for (i, conf) in enumerate(output_dict["detection_scores"]): # Tem classes tambem
        if conf >= 0.3:
            indexes.append(i)

    if len(indexes) > 0:
        newImg = cv2.cvtColor(newImg, cv2.COLOR_RGB2BGR)
        for i in indexes:
            conFaces.append(str(round(output_dict["detection_scores"][i], 2)))
            yI, xI, yF, xF = (output_dict["detection_boxes"][i]*np.array([h, w, h, w])).astype("int")
            faces.append(newImg[yI:yF, xI:xF])
            posFaces.append([xI, yI, xF, yF])
            # yT = yI - 10 if (yI - 10) > 0 else yI + 10
            # cv2.rectangle(newImg, (xI,yI), (xF, yF), (255, 0, 0), 1)
            # cv2.putText(newImg, confText, (xI, yT), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)

    return faces, conFaces, posFaces
